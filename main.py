﻿def sprawdzarka(lista):
    '''
Funkcja sprawdzająca, czy podana lista, spełnia kryteria zadania, tj.
czy zawiera tylko liczby naturalne nieujemne.
    '''
    for element in lista:
        # print(element,isinstance(element, int),type(element))
        if not isinstance(element, int):
            return False
            #raise ValueError("Lista zawiera element, który nie jest typu int.")
        if element < 0:
            return False
            #raise ValueError("Lista zawiera liczbę ujemną.")
    return True
#-------------------------------------------------------------------------------
def skrawarka(lista):
    '''
Najpierw odrzucam skrajne wartości, które są mniejsze lub równe niż te
głębiej, np. z zestawu {1,2,3,4,5,6,5,6,5,4,3,2,1} zostanie nam {6,5,6}.
Czyli jeśli wartość brzegowa jest mniejsza równa od tej dalej, to ją odrzuć,
bo nie będzie brana pod uwagę. Powtarzaj tak długo, aż nie będzie. To samo
dotyczy lewej i prawej strony.
    '''
    # skrawaj z lewej strony
    while lista[0] <= lista[1] and len(lista) > 2:
        if lista[0] <= lista[1]:
            del lista[0] # usun pierwszy element
    
    # skrawaj z prawej strony
    while lista[::-1][0] <= lista[::-1][1] and len(lista) > 2:
        if lista[::-1][0] <= lista[::-1][1]:
            del lista[-1] # usun ostatni element
    return lista
#-------------------------------------------------------------------------------
def zliczarka_v1(lista):
    '''
    Pierwsza wersja zliczarki, bazująca na graficznym poruszaniu strzałkami.
    Zlicza puste kratki w wierszu, ale można to robić łatwiej...
    Ta wersja nie potrzebowała funkcji zburzarka() i miała się przesuwać
    poziomami do góry.
    '''
    # 
    # Uwaga, lista musi wcześniej przejść przez funkcję skrawarka()
    # inaczej ta funkcja może nie zadziałać.
    # 
    SZEROKOSC = len(lista) # liczba liczb w tablicy, już po odrzuceniu tych
                           # skrajnych
    if SZEROKOSC < 3:
        return 0
    WSK_1 = lista[0]
    WSK_2 = lista[1]
    WSK_3 = lista[2]
    temp_suma = 0 # suma, którą dodamy do naszej SUMY, jeśli będzie spełniała
                  # kryteria
    suma = 0 # całkowita suma z całego wiersza
    
    obecny_poziom = 1
    def czy_czarna(kratka):
        return kratka >= obecny_poziom
    
    # poruszanie strzałkami:
    i = 0 # o tyle przesuwamy wszystkie strzałki, zawsze o 1 lub 2
    j = 0 # o tyle przesuwamy WSK_3
    while i <= SZEROKOSC-3:
        WSK_1 = lista[i]
        WSK_2 = lista[i+1]
        WSK_3 = lista[i+2]
        #print("--------------")
        #print("WSK =",WSK_1,WSK_2,WSK_3,"pozycje:",i,i+1,i+2)
        #print("i=",i,"j=",j)
        j = 0
        # sprawdzamy, czy WSK_2 jest czarna. Jeśli tak, to idziemy dalej o 1.
        if not czy_czarna(WSK_2): # 2. kratka jest pusta
            temp_suma = temp_suma + 1
            if czy_czarna(WSK_3): # 3. kratka jest czarna
                # jesli jest czarna, to "zamykamy" ciecz z dwóch stron
                suma = suma + temp_suma
                #print("suma=",suma)
                temp_suma = 0
                #print("WSK =",WSK_1,WSK_2,WSK_3,"pozycje:",i,i+1,i+2+j)
                i = i + 2 # o 2 w prawo, bo taka jest odl między WSK_1 a 3
            while not czy_czarna(WSK_3): # 3. kratka jest pusta
                #print("i=",i,"j=",j)
                temp_suma = temp_suma + 1
                #print("temp_suma=",temp_suma)
                j = j + 1 # idziemy o jeden w prawo
                #print("i+2+j =",i+2+j)
                WSK_3 = lista[i+2+j]
                if czy_czarna(WSK_3): # 3. kratka jest czarna
                    # jesli jest czarna, to "zamykamy" ciecz z dwóch stron
                    suma = suma + temp_suma
                    #print("suma=",suma)
                    temp_suma = 0
                    #print("WSK =",WSK_1,WSK_2,WSK_3,"pozycje:",i,i+1,i+2+j)
                    i = i + 2 + j
            continue # bo przesuwamy się o 2+j, nie o 1
        i = i + 1 # idziemy o 1 w prawo
    return suma
#-------------------------------------------------------------------------------
def zliczarka(lista):
    '''
    Druga wersja zliczarki.
    Wystarczy zliczać zera...
    '''
    #
    # Uwaga, lista musi wcześniej przejść przez funkcję skrawarka() oraz
    # wyburzarka(), inaczej ta funkcja może nie zadziałać.
    #
    SZEROKOSC = len(lista) # liczba liczb w tablicy, już po odrzuceniu tych
                           # skrajnych
    if SZEROKOSC < 3:
        return 0
    suma = 0 # całkowita suma z całego wiersza
    for elem in lista:
        if elem == 0:
            suma = suma + 1
    return suma
#-------------------------------------------------------------------------------
def wyburzarka(lista):
    '''
Funkcja wyburza dolny wiersz i zmienia wartości listy.
    '''
    for i in range(len(lista)):
        lista[i] = max(0,lista[i] - 1)
    return lista
#-------------------------------------------------------------------------------
def material(spaceShip):
    if len(spaceShip) < 3: # lista jest za krótka, nic z tego nie będzie
        return 0
    WYSOKOSC = max(spaceShip) # największa liczba występująca w zestawie,
                              # czyli wysokość najwyższego słupka
    obecny_poziom = 1 # wartości od 1 do WYSOKOSC
    suma = 0
    if sprawdzarka(spaceShip):
        # najpierw poziom 1, bez wyburzania:
        skrawarka(spaceShip)
        # print("po skrawaniu:",spaceShip,"\n")
        suma = suma + zliczarka(spaceShip)
        for poziom in range(obecny_poziom+1,WYSOKOSC+1):
            # print("obecny poziom:",poziom)
            wyburzarka(spaceShip)
            # print("po wyburzeniu:",spaceShip)
            skrawarka(spaceShip)
            # print("po skrawaniu:",spaceShip,"\n")
            suma = suma + zliczarka(spaceShip)
    else:
        return "Błąd. Lista powinna zawierać tylko liczby nieujemne."
    return suma
#-------------------------------------------------------------------------------
#przyklad = [0,1,0,2,1,0,1,3,2,1,3,1]
#print("wynik:",material(przyklad))
#przyklad2 = [6,2,1,1,8,0,5,5,0,1,8,9,6,9,4,8,0,0]
#print("wynik:",material(przyklad2))
