﻿from main import material

'''
PRZYKŁADOWE DANE I WYNIKI:
- [0,1,0,2,1,0,1,3,2,1,2,1] = 6 // przykład z obrazka
- [0,1,0,2,1,0,3,1,0,1,2] = 8   // przykład z obrazka
- [4,2,0,3,2,5] = 9             // przykład z obrazka
- [6,4,2,0,3,2,0,3,1,4,5,3,2,7,5,3,0,1,2,1,3,4,6,8,1,3] = 83 // przykład z polecenia
- [6,2,1,1,8,0,5,5,0,1,8,9,6,9,4,8,0,0] = 50 // przykład z polecenia
- [0,1,2,1,0,3,1,2,0] = 5 // przykład z animacji
- [0,3,2,0,3,2,0,4,2,0] = 8 // przykład z animacji

# minimalna poprawna lista, która zwróci niezerowy wynik
- [1,0,1] = 1

# proste przypadki testowe
- [0,1,0,1,0] = 1
- [0,1,0,0,0,1,0] = 3
- [1,2,3,4,5,6,5,6,5,4,3,2,1] = 1

# skrajne przypadki testowe
- [0] = 0
- [0,0] = 0
- [1,1] = 0
- [0,0,0,0,0,0,0] = 0
- [2,2,2,2,2,2,2] = 0
- [1,2,3,4,5,6,7] = 0
- [7,6,5,4,3,2,1] = 0
'''

assert material([6,2,1,1,8,0,5,5,0,1,8,9,6,9,4,8,0,0]) == 50
assert material([0,1,0,2,1,0,1,3,2,1,2,1]) == 6
assert material([0,1,0,2,1,0,3,1,0,1,2]) == 8
assert material([4,2,0,3,2,5]) == 9
assert material([6,4,2,0,3,2,0,3,1,4,5,3,2,7,5,3,0,1,2,1,3,4,6,8,1,3]) == 83
assert material([6,2,1,1,8,0,5,5,0,1,8,9,6,9,4,8,0,0]) == 50
assert material([0,1,0,2,1,0,3,1,2,0]) == 5
assert material([0,3,2,0,3,2,0,4,2,0]) == 8
assert material([1,0,1]) == 1
assert material([0,1,0,1,0]) == 1
assert material([0,1,0,0,0,1,0]) == 3
assert material([1,2,3,4,5,6,5,6,5,4,3,2,1]) == 1
assert material([0]) == 0
assert material([0,0]) == 0
assert material([1,1]) == 0
assert material([0,0,0,0,0,0,0]) == 0
assert material([2,2,2,2,2,2,2]) == 0
assert material([1,2,3,4,5,6,7]) == 0
assert material([7,6,5,4,3,2,1]) == 0

print("Wszystkie testy przeszły pomyślnie!")